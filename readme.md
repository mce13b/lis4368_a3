> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368: Advanced Web Application Development
## Marielle Estrugo

### Assignment 3 Requirements:
*Three Parts*

1. Git Repo

2. Database

3. Chapter Questions

##Part 1: Git Repo
[Git Repo Link](https://bitbucket.org/mce13b/lis4368_a3)

##Part 2: Database
![pet_store.png](https://bitbucket.org/repo/4Gn7K9/images/118661998-pet_store.png)


[SQL Doc](https://bitbucket.org/mce13b/lis4368_a3/src/b40b5d8f3c3b30c00f6cc89b992b9364bdffde20/docs/pet_store.sql?at=master&fileviewer=file-view-default)


[MWB File](https://bitbucket.org/mce13b/lis4368_a3/src/b40b5d8f3c3b30c00f6cc89b992b9364bdffde20/docs/pet_store.mwb?at=master&fileviewer=file-view-default)

##Part 3: Chapter Questions
![question_set_1.png](https://bitbucket.org/repo/4Gn7K9/images/2251502059-question_set_1.png)

![question_set_2.png](https://bitbucket.org/repo/4Gn7K9/images/1091934289-question_set_2.png)